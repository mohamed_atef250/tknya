package com.yumaas.tknyaapp.base;

import com.yumaas.tknyaapp.models.Ads;
import com.yumaas.tknyaapp.models.Book;
import com.yumaas.tknyaapp.models.Chat;
import com.yumaas.tknyaapp.models.Order;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;


public class DataLists {

    public ArrayList<User>  users = new ArrayList<>();
    public ArrayList<Book>  books = new ArrayList<>();
    public ArrayList<Chat>  chats = new ArrayList<>();
    public ArrayList<Ads>   ads = new ArrayList<>();
    public ArrayList<Book>  flowers = new ArrayList<>();
    public ArrayList<Order> orders = new ArrayList<>();

}
