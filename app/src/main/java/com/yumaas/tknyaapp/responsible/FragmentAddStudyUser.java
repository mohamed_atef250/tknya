package com.yumaas.tknyaapp.responsible;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.yumaas.tknyaapp.ImageResponse;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.SweetDialogs;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.base.Validate;
import com.yumaas.tknyaapp.base.filesutils.FileOperations;
import com.yumaas.tknyaapp.base.filesutils.VolleyFileObject;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.User;
import com.yumaas.tknyaapp.volleyutils.ConnectionHelper;
import com.yumaas.tknyaapp.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class FragmentAddStudyUser extends Fragment {
    View rootView;

    EditText name,email,phone,about,image,password;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_responsible2, container, false);

        name=rootView.findViewById(R.id.name);
        image=rootView.findViewById(R.id.image);
        phone=rootView.findViewById(R.id.phone);
        email  = rootView.findViewById(R.id.email);
        password = rootView.findViewById(R.id.password);
        about = rootView.findViewById(R.id.about);

        image.setFocusable(false);
        image.setClickable(true);

        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });

        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(validate()){
                    User user = new User(name.getText().toString(),
                            email.getText().toString()
                            ,
                            phone.getText().toString()
                            ,
                            password.getText().toString()

                    );
                    user.type=5;
                    user.about=about.getText().toString();
                    user.image = selectedImage;

                    DataBaseHelper.addUser(user);
                    SweetDialogs.successMessage(requireActivity(), "تم الاضافه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            FragmentHelper.popLastFragment(getActivity());
                        }
                    });
                }
            }
        });
        return rootView;
    }

    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())){
            name.setError("من فضلك املا الحقل");
            return false;
        }

        if(Validate.isEmpty(email.getText().toString())){
            email.setError("من فضلك املا الحقل");
            return false;
        }

        if(Validate.isEmpty(phone.getText().toString())){
            phone.setError("من فضلك املا الحقل");
            return false;
        }

        if(Validate.isEmpty(password.getText().toString())){
            password.setError("من فضلك املا الحقل");
            return false;
        }

        if(Validate.isEmpty(about.getText().toString())){
            about.setError("من فضلك املا الحقل");
            return false;
        }


        return true;
    }

    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(getActivity(), data, "image",
                        43);


        image.setText("تم اضافه الهويه بنجاح");

        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }



}
