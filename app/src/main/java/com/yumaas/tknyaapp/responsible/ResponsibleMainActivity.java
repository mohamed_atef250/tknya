package com.yumaas.tknyaapp.responsible;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.yumaas.tknyaapp.admin.FragmentStudyTeachers;
import com.yumaas.tknyaapp.admin.FragmentTeachers;
import com.yumaas.tknyaapp.main.ChattersFragment;
import com.yumaas.tknyaapp.main.ChattersFragment3;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.main.UpdateProfileActivity;
import com.yumaas.tknyaapp.students.FragmentAds;
import com.yumaas.tknyaapp.students.StudentsMainActivity;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import me.ibrahimsn.lib.NiceBottomBar;

public class ResponsibleMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_responsible);
        FragmentHelper.addFragment(ResponsibleMainActivity.this,new ChattersFragment(),"ChattersFragment");

        findViewById(R.id.iv_main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.iv_main_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ResponsibleMainActivity.this, UpdateProfileActivity.class));
            }
        });



        NiceBottomBar bottomBar = (NiceBottomBar) findViewById(R.id.bottomBar);

        bottomBar.setOnItemSelected(new Function1<Integer, Unit>() {
            @Override
            public Unit invoke(Integer integer) {

                if(integer==0){
                    FragmentHelper.addFragment(ResponsibleMainActivity.this,new ChattersFragment3(),"ChattersFragment");
                }else if(integer==1){
                    FragmentHelper.addFragment(ResponsibleMainActivity.this,new FragmentAds(),"FragmentAddAds");
                }else {
                    FragmentHelper.addFragment(ResponsibleMainActivity.this,new FragmentStudyTeachers(),"FragmentProfile");
                }

                return null;
            }
        });


    }
}