package com.yumaas.tknyaapp.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.Chat;
import com.yumaas.tknyaapp.models.User;
import java.util.ArrayList;
import java.util.HashMap;


public class ChattersFragment extends Fragment {
    View rootView;
    RecyclerView menuList;
    HashMap<String,Boolean  >hashMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);


        menuList = rootView.findViewById(R.id.menu_list);

        hashMap = new HashMap<>();

        ArrayList<Chat> chatters = new ArrayList<>();

        ArrayList<Chat> chattersTemp = DataBaseHelper.getDataLists().chats;
        User user = DataBaseHelper.getSavedUser();


        for(int i=0; i<chattersTemp.size(); i++){

            if(user.id == chattersTemp.get(i).sender.id ||
                    (user.id == chattersTemp.get(i).reciver.id)){
                String text = Math.max(chattersTemp.get(i).sender.id,chattersTemp.get(i).reciver.id)+""+
                Math.min(chattersTemp.get(i).reciver.id,chattersTemp.get(i).sender.id);

                if(!hashMap.containsKey(text)){
                    chatters.add(chattersTemp.get(i));
                }

                hashMap.put(text,true);
            }

        }


        ChattersAdapter chatAdapter = new ChattersAdapter(getActivity(),chatters);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
