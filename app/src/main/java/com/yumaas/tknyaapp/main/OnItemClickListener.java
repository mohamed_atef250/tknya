package com.yumaas.tknyaapp.main;

public interface OnItemClickListener {
    public void onItemClickListener(int position);
}
