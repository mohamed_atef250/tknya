package com.yumaas.tknyaapp.main;


import android.Manifest;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.SweetDialogs;
import com.yumaas.tknyaapp.admin.AdminMainActivity;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.databinding.FragmentLoginBinding;
import com.yumaas.tknyaapp.models.User;
import com.yumaas.tknyaapp.responsible.ResponsibleMainActivity;
import com.yumaas.tknyaapp.students.StudentsMainActivity;


public class LoginActivity extends AppCompatActivity {

    FragmentLoginBinding fragmentLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_login);

        if(DataBaseHelper.isFirstTime()) {
            User user = new User("مدير التطبيق", "admin@admin.com", "123456", "123456");
            user.type=0;
            user.image="https://img.freepik.com/free-vector/admin-concept-illustration_114360-2139.jpg?size=338&ext=jpg";
            DataBaseHelper.addUser(user);
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CALL_PHONE}, 342);
        }

        fragmentLoginBinding = DataBindingUtil.setContentView(this, R.layout.fragment_login);

        fragmentLoginBinding.btnLoginSignIn.setOnClickListener(view -> {

                User user = DataBaseHelper.loginUser(fragmentLoginBinding.email.getText().toString()
                        , fragmentLoginBinding.password.getText().toString());
                if (user != null) {
                    DataBaseHelper.saveStudent(user);

                    Intent intent = new Intent(LoginActivity.this, ResponsibleMainActivity.class);

                    if(user.type==0){
                        intent = new Intent(LoginActivity.this, AdminMainActivity.class);
                    }else if(user.type==2){
                        intent = new Intent(LoginActivity.this, StudentsMainActivity.class);
                    }
                    startActivity(intent);
                    finish();
                } else {
                    SweetDialogs.errorMessage(LoginActivity.this, "ناسف البريد الالكتروني او كلمه السر خطا");
                }

        });


        fragmentLoginBinding.tvLoginRegister.setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, RegisterActivity.class)));


    }


}
