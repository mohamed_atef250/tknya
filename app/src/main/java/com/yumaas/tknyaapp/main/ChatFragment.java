package com.yumaas.tknyaapp.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.Chat;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;


public class ChatFragment extends Fragment {
    private View rootView;
    private RecyclerView menuList;
    ArrayList<Chat> chatters, chats;
    EditText nameEditText;
    Button chatBtn;
    User reciver,user;
    ChatAdapter chatAdapter;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chat, container, false);

        menuList = rootView.findViewById(R.id.menu_list);

        nameEditText = rootView.findViewById(R.id.et_chat_message);
        chatBtn = rootView.findViewById(R.id.btn_chat);


        chats = new ArrayList<>();
        chatters = DataBaseHelper.getDataLists().chats;
        user = DataBaseHelper.getSavedUser();


        Chat chat = (Chat) getArguments().getSerializable("chatItem");


        if(user.id==chat.sender.id){
            reciver=chat.reciver;
        }else {
            reciver=chat.sender;
        }

        for (int i = 0; i < chatters.size(); i++) {

            if ((chatters.get(i).sender.id == chat.sender.id &&
                    chatters.get(i).reciver.id == chat.reciver.id
            ) || (chatters.get(i).reciver.id == chat.sender.id &&
                    chatters.get(i).sender.id == chat.reciver.id)) ;
            chats.add(chatters.get(i));
        }
         chatAdapter = new ChatAdapter(getActivity(), chats);
        menuList.setAdapter(chatAdapter);


        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Chat chat = new Chat(user,reciver,nameEditText.getText().toString());
                chats.add(chat);
                DataBaseHelper.addChat(chat);
                chatAdapter.notifyDataSetChanged();
            }
        });

        return rootView;
    }


}
