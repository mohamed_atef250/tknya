package com.yumaas.tknyaapp.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.yumaas.tknyaapp.ImageResponse;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.SweetDialogs;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.base.Validate;
import com.yumaas.tknyaapp.base.filesutils.FileOperations;
import com.yumaas.tknyaapp.base.filesutils.VolleyFileObject;
import com.yumaas.tknyaapp.databinding.FragmentRegisterBinding;
import com.yumaas.tknyaapp.models.User;
import com.yumaas.tknyaapp.students.StudentsMainActivity;
import com.yumaas.tknyaapp.volleyutils.ConnectionHelper;
import com.yumaas.tknyaapp.volleyutils.ConnectionListener;


import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class RegisterActivity extends AppCompatActivity {

    FragmentRegisterBinding fragmentRegisterBinding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fragmentRegisterBinding = DataBindingUtil.setContentView(this, R.layout.fragment_register);

        fragmentRegisterBinding.image.setFocusable(false);
        fragmentRegisterBinding.image.setClickable(true);

        fragmentRegisterBinding.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(
                        Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "اختر صوره"), 242);
            }
        });

        fragmentRegisterBinding.register.setOnClickListener(view -> {
            if(validate()){
                User user = new User(fragmentRegisterBinding.name.getText().toString(),
                        fragmentRegisterBinding.email.getText().toString(),
                        fragmentRegisterBinding.phone.getText().toString(),
                        fragmentRegisterBinding.password.getText().toString());
                user.type=2;
                user.image=selectedImage;
                DataBaseHelper.addUser(user);
                DataBaseHelper.saveStudent(user);
                SweetDialogs.successMessage(com.yumaas.tknyaapp.main.RegisterActivity.this, "تم التسجيل بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        Intent intent = new Intent( RegisterActivity.this, StudentsMainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
    }




    private boolean validate(){

        if(Validate.isEmpty(fragmentRegisterBinding.name.getText().toString())){
            fragmentRegisterBinding.name.setError("ادخل الاسم");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.email.getText().toString())){
            fragmentRegisterBinding.email.setError("ادخل البريد الالكتروني");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.phone.getText().toString())){
            fragmentRegisterBinding.phone.setError("ادخل رقم الهاتف");
            return false;
        }else if(Validate.isEmpty(fragmentRegisterBinding.password.getText().toString())){
            fragmentRegisterBinding.password.setError("ادخل كلمه المرور");
            return false;
        }else if(!Validate.isMail(fragmentRegisterBinding.email.getText().toString())){
            fragmentRegisterBinding.email.setError("البريد الالكتروني خاطئ");
            return false;
        }

        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                FileOperations.getVolleyFileObject(this, data, "image",
                        43);


        fragmentRegisterBinding.image.setText("تم اضافه الهويه بنجاح");

        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }


    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                selectedImage = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }







}
