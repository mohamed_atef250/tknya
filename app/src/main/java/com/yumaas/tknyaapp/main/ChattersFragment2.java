package com.yumaas.tknyaapp.main;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.ArrayRes;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.Chat;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;
import java.util.HashMap;


public class ChattersFragment2 extends Fragment {
    View rootView;
    RecyclerView menuList;
    HashMap<String,Boolean  >hashMap;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_chatters, container, false);


        menuList = rootView.findViewById(R.id.menu_list);

        hashMap = new HashMap<>();


        ArrayList<Chat> chattersTemp = new ArrayList<>();
        ArrayList<User> users = DataBaseHelper.getDataLists().users;
        User user = DataBaseHelper.getSavedUser();


        for(int i=0; i<users.size(); i++){
            if(users.get(i).type==0||users.get(i).type==1) {
                Chat chat = new Chat(users.get(i), user, "");
                chattersTemp.add(chat);
            }
        }


        ChattersAdapter chatAdapter = new ChattersAdapter(getActivity(),chattersTemp);
        menuList.setAdapter(chatAdapter);


        return rootView;
    }


}
