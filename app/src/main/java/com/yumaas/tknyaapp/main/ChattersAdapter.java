package com.yumaas.tknyaapp.main;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.models.Chat;

import java.util.ArrayList;


public class ChattersAdapter extends RecyclerView.Adapter<ChattersAdapter.ViewHolder> {

    Context context;
    ArrayList<Chat>chattersList;


    public ChattersAdapter(Context context, ArrayList<Chat>chattersList) {
        this.context = context;
        this.chattersList=chattersList;
    }


    @Override
    public ChattersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_chatter, parent, false);
        ChattersAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ChattersAdapter.ViewHolder holder, final int position) {

        holder.message.setText(chattersList.get(position).sender.name);


        Picasso.get().load(chattersList.get(position).sender.image).into(holder.image);


        holder.itemView.setOnClickListener(view -> {

            ChatFragment chatFragment =   new ChatFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("chatItem",chattersList.get(position));
            chatFragment.setArguments(bundle);
            FragmentHelper.addFragment(view.getContext(),chatFragment, "ChatFragment");
        });

    }

    @Override
    public int getItemCount() {
        return chattersList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        LinearLayout chatterLayout;
        TextView message;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            chatterLayout = itemView.findViewById(R.id.chatterLayout);
            message = itemView.findViewById(R.id.tv_review_reviewer);
            image = itemView.findViewById(R.id.image);
        }
    }
}
