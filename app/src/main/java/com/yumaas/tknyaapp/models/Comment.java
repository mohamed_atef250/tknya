package com.yumaas.tknyaapp.models;

import com.yumaas.tknyaapp.base.DataBaseHelper;

import java.io.Serializable;

public class Comment implements Serializable {
    public int id;
    public String comment;
    public User user;

    public Comment(String comment, User user){
        this.id= DataBaseHelper.generateId();
        this.comment=comment;
        this.user=user;
    }

}
