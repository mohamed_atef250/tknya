package com.yumaas.tknyaapp.models;

import java.io.Serializable;

public class Chat implements Serializable {

    public User sender;
    public User reciver;
    public String message;

    public Chat(User sender,User reciver,String message){
        this.sender=sender;
        this.reciver=reciver;
        this.message=message;
    }

}
