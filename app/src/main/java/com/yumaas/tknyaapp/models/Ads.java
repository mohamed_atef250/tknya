package com.yumaas.tknyaapp.models;

import com.yumaas.tknyaapp.base.DataBaseHelper;

import java.io.Serializable;
import java.util.ArrayList;

public class Ads implements Serializable {
    public int id;
    public String title,details,image;
    public ArrayList<Comment>comments=new ArrayList<>();

    public Ads(String title,String details,String image){
        this.id= DataBaseHelper.generateId();
        this.title=title;
        this.details=details;
        this.image=image;
    }

}
