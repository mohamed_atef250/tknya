package com.yumaas.tknyaapp.admin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.yumaas.tknyaapp.main.ChattersFragment;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.students.FragmentBooks;

import me.ibrahimsn.lib.NiceBottomBar;

public class AdminMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_activity_main);
        findViewById(R.id.iv_main_profile).setVisibility(View.GONE);
        findViewById(R.id.iv_main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        FragmentHelper.addFragment(AdminMainActivity.this,new FragmentTeachers(),"FragmentTeachers");

        NiceBottomBar bottomBar =  findViewById(R.id.bottomBar);

        bottomBar.setOnItemSelected(integer -> {

            if(integer==0){
                FragmentHelper.addFragment(AdminMainActivity.this,new FragmentTeachers(),"FragmentTeachers");
            }else if(integer==1){
                FragmentHelper.addFragment(AdminMainActivity.this,new FragmentStudents(),"FragmentStudents");
            }else if(integer==2){
                FragmentHelper.addFragment(AdminMainActivity.this,new FragmentBooks(),"FragmentBooks");
            }else {
                FragmentHelper.addFragment(AdminMainActivity.this,new ChattersFragment(),"ChattersFragment");
            }

            return null;
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}