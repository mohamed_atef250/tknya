package com.yumaas.tknyaapp.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AdminTeachersAdapter extends RecyclerView.Adapter<AdminTeachersAdapter.ViewHolder> {

    Context context;
    ArrayList<User>users;

    public AdminTeachersAdapter(Context context,ArrayList<User>users) {
        this.context = context;
        this.users=users;
    }


    @Override
    public AdminTeachersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_teacher, parent, false);
        AdminTeachersAdapter.ViewHolder viewHolder = new AdminTeachersAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdminTeachersAdapter.ViewHolder holder, final int position) {


            Picasso.get().load(users.get(position).image).into(holder.image);

            holder.delete.setOnClickListener(view -> new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("هل تريد المسح")
                    .setContentText("بالتاكيد مسح هذا العنصر ؟")
                    .setConfirmText("نعم امسح").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    DataBaseHelper.removeUser(users.get(position));
                    users.remove(position);
                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    }catch (Exception e){
                        e.getStackTrace();
                    }

                }
            })
                    .show());

            holder.name.setText(users.get(position).name);

     }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        Button delete;
        TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            delete = itemView.findViewById(R.id.delete);
            name = itemView.findViewById(R.id.name);
        }
    }
}
