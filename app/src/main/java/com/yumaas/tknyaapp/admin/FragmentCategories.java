package com.yumaas.tknyaapp.admin;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.databinding.FragmentListBinding;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;


public class FragmentCategories extends Fragment {
    View rootView;
    FragmentListBinding fragmentListBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentListBinding  = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);


        fragmentListBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new FragmentAddResponsible(),"FragmentAddResponsible");
            }
        });

        RecyclerView recyclerView =  fragmentListBinding.recyclerView;




        AdminBooksAdapter menuAdapter = new AdminBooksAdapter(getActivity(),DataBaseHelper.getDataLists().books);
        recyclerView.setAdapter(menuAdapter);


        return rootView;
    }


}
