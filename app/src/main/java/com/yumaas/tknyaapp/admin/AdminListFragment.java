package com.yumaas.tknyaapp.admin;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.databinding.FragmentListBinding;


public class AdminListFragment extends Fragment {
    private View rootView;
    FragmentListBinding fragmentListBinding;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentListBinding  = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);

        fragmentListBinding.tvSystemsAdd.setVisibility(View.VISIBLE);
        fragmentListBinding.btnAdd.setText("اضافه ورده");

        fragmentListBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
             }
        });

        RecyclerView recyclerView =  fragmentListBinding.recyclerView;

        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));

//        AdminFlowersAdapter categoriesAdapter1 = new AdminFlowersAdapter(new OnItemClickListener() {
//            @Override
//            public void onItemClickListener(int position) {
//
//            }
//        }, DataBaseHelper.getDataLists().flowers);
//        recyclerView.setAdapter(categoriesAdapter1);
        rootView=fragmentListBinding.getRoot();
        return rootView;
    }

}