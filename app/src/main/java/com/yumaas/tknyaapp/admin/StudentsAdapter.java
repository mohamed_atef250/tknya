package com.yumaas.tknyaapp.admin;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.SweetDialogs;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class StudentsAdapter extends RecyclerView.Adapter<StudentsAdapter.ViewHolder> {

    Context context;
    ArrayList<User> users;

    public StudentsAdapter(Context context, ArrayList<User> users) {
        this.context = context;
        this.users = users;
    }


    @Override
    public StudentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_admin_students, parent, false);
        StudentsAdapter.ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StudentsAdapter.ViewHolder holder, final int position) {


        Picasso.get().load(users.get(position).image).into(holder.image);

        holder.name.setText(users.get(position).name);

        if (users.get(position).accepted == 1) {
            holder.accept.setVisibility(View.GONE);
        } else {
            holder.accept.setVisibility(View.VISIBLE);
        }

        holder.accept.setOnClickListener(view -> {

            SweetDialogs.successMessage(view.getContext(), "تم الموافقه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {
                    users.get(position).accepted = 1;
                    DataBaseHelper.updateUser(users.get(position));
                    notifyDataSetChanged();
                }
            });
        });

        holder.delete.setOnClickListener(view -> {
            new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("هل تريد المسح")
                    .setContentText("بالتاكيد مسح هذا الطالب ؟")
                    .setConfirmText("نعم امسح").setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                @Override
                public void onClick(SweetAlertDialog sweetAlertDialog) {

                    DataBaseHelper.removeUser(users.get(position));
                    users.remove(position);
                    notifyDataSetChanged();

                    try {
                        sweetAlertDialog.cancel();
                        sweetAlertDialog.dismiss();
                    } catch (Exception e) {
                        e.getStackTrace();
                    }

                }
            })
                    .show();
        });

    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        TextView name;
        Button accept, delete;

        public ViewHolder(View itemView) {
            super(itemView);

            image = itemView.findViewById(R.id.image);
            name = itemView.findViewById(R.id.name);
            accept = itemView.findViewById(R.id.accept);
            delete = itemView.findViewById(R.id.delete);
        }
    }
}
