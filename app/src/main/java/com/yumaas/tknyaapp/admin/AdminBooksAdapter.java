package com.yumaas.tknyaapp.admin;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.Book;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class AdminBooksAdapter extends RecyclerView.Adapter<AdminBooksAdapter.ViewHolder> {

    Context context;
    ArrayList<Book>books;

    public boolean showBook;

    public AdminBooksAdapter(Context context,ArrayList<Book>books) {
        this.context = context;
        this.books=books;
    }


    @Override
    public AdminBooksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false);
        AdminBooksAdapter.ViewHolder viewHolder = new AdminBooksAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NotNull AdminBooksAdapter.ViewHolder holder, final int position) {

        holder.name.setText(books.get(position).name);


        if(showBook){
            holder.cardview.setVisibility(View.VISIBLE);
        }else {
            holder.cardview.setVisibility(View.GONE);
        }

        holder.delete.setOnClickListener(view -> {

            new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("هل تريد المسح")
                    .setContentText("بالتاكيد مسح هذا الكتاب ؟")
                    .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {

                        DataBaseHelper.removeBook(books.get(position));
                        books.remove(position);
                        notifyDataSetChanged();

                        try {
                            sweetAlertDialog.cancel();
                            sweetAlertDialog.dismiss();
                        }catch (Exception e){
                            e.getStackTrace();
                        }

                    }).show();
        });

        holder.download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(books.get(position).book));
                view.getContext().startActivity(browserIntent);
            }
        });

     }

    @Override
    public int getItemCount() {
        return books.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView name;
        Button download,delete;
        CardView cardview;

        public ViewHolder(View itemView) {
            super(itemView);
            download = itemView.findViewById(R.id.download);
            delete = itemView.findViewById(R.id.delete);
            name=itemView.findViewById(R.id.name);
            cardview=itemView.findViewById(R.id.cardview);
        }
    }
}
