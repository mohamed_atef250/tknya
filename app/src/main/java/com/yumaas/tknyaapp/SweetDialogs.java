package com.yumaas.tknyaapp;

import android.content.Context;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class SweetDialogs {

    public static void successMessage(Context context, String message, SweetAlertDialog.OnSweetClickListener onSweetClickListener) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.SUCCESS_TYPE);
        sweetAlertDialog.setTitleText("تمت بنجاح");
        sweetAlertDialog.setContentText(message).setConfirmButton("تم", new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                onSweetClickListener.onClick(sweetAlertDialog);
                sweetAlertDialog.cancel();
            }
        });
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setCanceledOnTouchOutside(false);
        sweetAlertDialog.show();
    }

    public static void errorMessage(Context context, String message) {
        SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(context, SweetAlertDialog.ERROR_TYPE);
        sweetAlertDialog.setTitleText("خطا");
        sweetAlertDialog.setConfirmText("تم");
        sweetAlertDialog.setContentText(message);
        sweetAlertDialog.setCancelable(false);
        sweetAlertDialog.setCanceledOnTouchOutside(false);
        sweetAlertDialog.show();
    }



}
