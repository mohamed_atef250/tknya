package com.yumaas.tknyaapp.students;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class TeacherMembersAdapter extends RecyclerView.Adapter<TeacherMembersAdapter.ViewHolder> {

    Context context;
    ArrayList<User>users;
    boolean showDelete=false;
    public TeacherMembersAdapter(Context context, ArrayList<User>users, boolean showDelete) {
        this.context = context;
        this.users=users;
        this.showDelete=showDelete;
    }


    @Override
    public TeacherMembersAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_responsible, parent, false);
        TeacherMembersAdapter.ViewHolder viewHolder = new TeacherMembersAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(TeacherMembersAdapter.ViewHolder holder, final int position) {


        holder.name.setText(users.get(position).name);
        holder.about.setText(users.get(position).about);

        Picasso.get().load(users.get(position).image).into(holder.image);

        holder.cardcallphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + users.get(position).phone));
                    view.getContext().startActivity(intent);
                }catch (Exception e){
                    e.getStackTrace();
                }
            }
        });

        if(showDelete){
            holder.carddelete.setVisibility(View.VISIBLE);
        }else {
            holder.carddelete.setVisibility(View.INVISIBLE);
        }
            holder.delete.setOnClickListener(view -> new SweetAlertDialog(view.getContext(), SweetAlertDialog.WARNING_TYPE)
                    .setTitleText("هل تريد المسح")
                    .setContentText("بالتاكيد مسح هذا العنصر ؟")
                    .setConfirmText("نعم امسح").setConfirmClickListener(sweetAlertDialog -> {

                        DataBaseHelper.removeUser(users.get(position));
                        users.remove(position);
                        notifyDataSetChanged();

                        try {
                            sweetAlertDialog.cancel();
                            sweetAlertDialog.dismiss();
                        }catch (Exception e){
                            e.getStackTrace();
                        }

                    })
                    .show());

            holder.name.setText(users.get(position).name);

     }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView image;
        Button delete;
        CardView  carddelete;
        Button cardcallphone;

        TextView name,about;

        public ViewHolder(View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.image);
            delete = itemView.findViewById(R.id.delete);
            name = itemView.findViewById(R.id.name);
            about=itemView.findViewById(R.id.about);
            cardcallphone = itemView.findViewById(R.id.callphone);
            carddelete = itemView.findViewById(R.id.carddelete);
        }
    }
}
