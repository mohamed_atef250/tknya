package com.yumaas.tknyaapp.students;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;


public class StudentsCategoriesAdapter extends RecyclerView.Adapter<StudentsCategoriesAdapter.ViewHolder> {

    Context context;


    public StudentsCategoriesAdapter(Context context) {
        this.context = context;


    }


    @Override
    public StudentsCategoriesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_student_category, parent, false);
        StudentsCategoriesAdapter.ViewHolder viewHolder = new StudentsCategoriesAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(StudentsCategoriesAdapter.ViewHolder holder, final int position) {
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                view.getContext().startActivity(new Intent(view.getContext(), StudentsMainActivity.class));

            }
        });
     }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View itemView) {
            super(itemView);





        }
    }
}
