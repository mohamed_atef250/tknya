package com.yumaas.tknyaapp.students;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;


import com.yumaas.tknyaapp.ImageResponse;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.SweetDialogs;

import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.base.Validate;
import com.yumaas.tknyaapp.base.filesutils.CompressObject;
import com.yumaas.tknyaapp.base.filesutils.FileOperations;
import com.yumaas.tknyaapp.base.filesutils.VolleyFileObject;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.Book;
import com.yumaas.tknyaapp.volleyutils.ConnectionHelper;
import com.yumaas.tknyaapp.volleyutils.ConnectionListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;


public class FragmentAddBook extends Fragment {
    View rootView;
    EditText name,link;
    String bookLink;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_books, container, false);

        name=rootView.findViewById(R.id.name);
        link=rootView.findViewById(R.id.link);

        link.setFocusable(false);
        link.setClickable(true);

        link.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilePickerBuilder.getInstance()
                        .setMaxCount(1) //optional
                        .setActivityTheme(R.style.LibAppTheme) //optional
                        .pickFile(requireActivity());
            }
        });

        rootView.findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    DataBaseHelper.addBook(new Book(name.getText().toString(),bookLink));
                    SweetDialogs.successMessage(requireActivity(), "تم الاضافه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            FragmentHelper.popLastFragment(getActivity());

                        }
                    });
                }
            }
        });


        return rootView;
    }

    private boolean validate(){
        if(Validate.isEmpty(name.getText().toString())){
            name.setError("من فضلك املا الحقل");
            return false;
        }

        if(Validate.isEmpty(link.getText().toString())){
            link.setError("من فضلك املا الحقل");
            return false;
        }
        return true;
    }


    ArrayList<VolleyFileObject> volleyFileObjects;
    List<Uri> docPaths;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        docPaths = new ArrayList<>();
        docPaths.addAll(data.getParcelableArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS));


        volleyFileObjects = new ArrayList<>();
        VolleyFileObject volleyFileObject =
                new VolleyFileObject("image",docPaths.get(0).getPath(),1);


        CompressObject compressObject = new CompressObject();
        String filePath = FileOperations.getPath(getActivity(), docPaths.get(0));
        compressObject.setBytes(FileOperations.fileToBytes(filePath));
        volleyFileObject.setCompressObject(compressObject);
        link.setText("تم اضافه الكتاب");


        volleyFileObjects.add(volleyFileObject);



        addServiceApi();


    }



    String selectedImage="";

    private void addServiceApi() {
        ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setTitle("جاري التحميل");
        progressDialog.show();

        HashMap<String, String> params = new HashMap<>();

        new ConnectionHelper(new ConnectionListener() {
            @Override
            public void onRequestSuccess(Object response) {
                super.onRequestSuccess(response);
                ImageResponse imageResponse = (ImageResponse) response;

                bookLink = imageResponse.getState();

                Log.e("DAta",""+selectedImage);

                progressDialog.dismiss();
                progressDialog.cancel();
            }

            @Override
            public void onRequestError(Object error) {
                super.onRequestError(error);
            }
        }).multiPartConnect(params, volleyFileObjects, ImageResponse.class);
    }





}
