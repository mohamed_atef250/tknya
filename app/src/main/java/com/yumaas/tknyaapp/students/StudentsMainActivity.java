package com.yumaas.tknyaapp.students;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.yumaas.tknyaapp.admin.FragmentTeachers;
import com.yumaas.tknyaapp.main.ChattersFragment;
import com.yumaas.tknyaapp.main.ChattersFragment2;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.main.UpdateProfileActivity;

import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import me.ibrahimsn.lib.NiceBottomBar;

public class StudentsMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.student_activity_main);
        FragmentHelper.addFragment(StudentsMainActivity.this,new FragmentAds(),"FragmentAds");


        findViewById(R.id.iv_main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        findViewById(R.id.iv_main_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               startActivity(new Intent(StudentsMainActivity.this, UpdateProfileActivity.class));
            }
        });


        NiceBottomBar bottomBar = (NiceBottomBar) findViewById(R.id.bottomBar);

        bottomBar.setOnItemSelected(new Function1<Integer, Unit>() {
            @Override
            public Unit invoke(Integer integer) {
                if(integer==0){
                    FragmentHelper.addFragment(StudentsMainActivity.this,new FragmentAds(),"FragmentAds");
                }else if(integer==1){
                    FragmentHelper.addFragment(StudentsMainActivity.this,new StudentFragmentBooks(),"FragmentBooks");
                }else if(integer==2){
                    FragmentHelper.addFragment(StudentsMainActivity.this,new FragmentTeacherMembers(),"FragmentQuestions");
                }else {
                    FragmentHelper.addFragment(StudentsMainActivity.this,new ChattersFragment2(),"ChattersFragment");
                }

                return null;
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }


}