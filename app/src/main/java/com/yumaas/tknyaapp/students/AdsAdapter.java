package com.yumaas.tknyaapp.students;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.Ads;

import java.util.ArrayList;


public class AdsAdapter extends RecyclerView.Adapter<AdsAdapter.ViewHolder> {

    Context context;
    ArrayList<Ads>adsArrayList;

    public AdsAdapter(Context context, ArrayList<Ads>adsArrayList) {
        this.context = context;
        this.adsArrayList=adsArrayList;
    }


    @Override
    public AdsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ads, parent, false);
        AdsAdapter.ViewHolder viewHolder = new AdsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(AdsAdapter.ViewHolder holder, final int position) {
                holder.name.setText(adsArrayList.get(position).title);
        holder.details.setText(adsArrayList.get(position).details);
        Picasso.get().load(adsArrayList.get(position).image).into(holder.image);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ads",adsArrayList.get(position));
                FragmentComments fragmentComments = new FragmentComments();
                fragmentComments.setArguments(bundle);
                FragmentHelper.addFragment(view.getContext(),fragmentComments,"FragmentComments");
            }
        });
     }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name,details;
        ImageView image;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
            this.image=itemView.findViewById(R.id.image);
        }
    }
}
