package com.yumaas.tknyaapp.students;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.admin.AdminBooksAdapter;
import com.yumaas.tknyaapp.admin.StudentsAdapter;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.databinding.FragmentListBinding;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.Ads;

import java.util.ArrayList;

import static android.view.View.OnClickListener;


public class FragmentAds extends Fragment {
    View rootView;
    FragmentListBinding fragmentListBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        fragmentListBinding  = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);

        fragmentListBinding.tvSystemsAdd.setVisibility(View.VISIBLE);

        fragmentListBinding.btnAdd.setText("اضافه اعلان");

        fragmentListBinding.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentHelper.addFragment(getActivity(),new FragmentAddAds(),"FragmentAddAds");
            }
        });

        RecyclerView recyclerView =  fragmentListBinding.recyclerView;

        AdsAdapter adsAdapter = new AdsAdapter(getActivity(),DataBaseHelper.getDataLists().ads);
        recyclerView.setAdapter(adsAdapter);


        rootView=fragmentListBinding.getRoot();



        return rootView;
    }


}
