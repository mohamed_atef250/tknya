package com.yumaas.tknyaapp.students;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;


public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder> {

    Context context;


    public BooksAdapter(Context context) {
        this.context = context;
    }


    @Override
    public BooksAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false);
        BooksAdapter.ViewHolder viewHolder = new BooksAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(BooksAdapter.ViewHolder holder, final int position) {
     }

    @Override
    public int getItemCount() {
        return 10;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {



        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
