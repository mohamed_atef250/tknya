package com.yumaas.tknyaapp.students;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.SweetDialogs;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.base.Validate;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.Ads;
import com.yumaas.tknyaapp.models.Book;
import com.yumaas.tknyaapp.models.Comment;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class FragmentAddComment extends Fragment {
    View rootView;
    EditText comment;
    User user;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_comment, container, false);

        comment=rootView.findViewById(R.id.comment);
        user = DataBaseHelper.getSavedUser();
        Ads ads = (Ads) getArguments().getSerializable("ads");

        rootView.findViewById(R.id.add).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()){
                    ArrayList<Comment>comments = ads.comments;
                    comments.add(new Comment(comment.getText().toString(),user));
                    DataBaseHelper.updateAds(ads);
                    SweetDialogs.successMessage(requireActivity(), "تم الاضافه بنجاح", new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            FragmentHelper.popLastFragment(getActivity());
                        }
                    });
                }
            }
        });


        return rootView;
    }

    private boolean validate(){
        if(Validate.isEmpty(comment.getText().toString())){
            comment.setError("من فضلك املا الحقل");
            return false;
        }


        return true;
    }


}
