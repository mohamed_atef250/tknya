package com.yumaas.tknyaapp.students;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;

import static android.view.View.OnClickListener;


public class FragmentAddBooks extends Fragment {
    View rootView;
    RecyclerView menuList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add_books, container, false);
        menuList = rootView.findViewById(R.id.rv_systems_list);
        BooksAdapter menuAdapter = new BooksAdapter(getActivity());
        menuList.setAdapter(menuAdapter);

        rootView.findViewById(R.id.tv_systems_add).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        return rootView;
    }


}
