package com.yumaas.tknyaapp.students;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.admin.AdminBooksAdapter;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.databinding.FragmentListBinding;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.Ads;

import static android.view.View.OnClickListener;


public class FragmentComments extends Fragment {
    View rootView;
    FragmentListBinding fragmentListBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentListBinding  = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);

        fragmentListBinding.tvSystemsAdd.setVisibility(View.VISIBLE);
        fragmentListBinding.btnAdd.setText("اضافه تعليق");
        Ads ads = (Ads) getArguments().getSerializable("ads");

        fragmentListBinding.btnAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("ads",ads);
                FragmentAddComment fragmentComments = new FragmentAddComment();
                fragmentComments.setArguments(bundle);
                FragmentHelper.addFragment(getActivity(),fragmentComments,"FragmentAddComment");
            }
        });

        RecyclerView recyclerView =  fragmentListBinding.recyclerView;



        CommentsAdapter menuAdapter = new CommentsAdapter(getActivity(),ads.comments);
        recyclerView.setAdapter(menuAdapter);

        rootView = fragmentListBinding.getRoot();


        return rootView;
    }


}
