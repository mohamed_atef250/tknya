package com.yumaas.tknyaapp.students;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.main.FragmentHelper;
import com.yumaas.tknyaapp.models.Ads;
import com.yumaas.tknyaapp.models.Comment;

import java.util.ArrayList;


public class CommentsAdapter extends RecyclerView.Adapter<CommentsAdapter.ViewHolder> {

    Context context;
    ArrayList<Comment>adsArrayList;

    public CommentsAdapter(Context context, ArrayList<Comment>adsArrayList) {
        this.context = context;
        this.adsArrayList=adsArrayList;
    }


    @Override
    public CommentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        CommentsAdapter.ViewHolder viewHolder = new CommentsAdapter.ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(CommentsAdapter.ViewHolder holder, final int position) {
       holder.name.setText(" تعليق بواسطه " +adsArrayList.get(position).user.name);
        holder.details.setText(adsArrayList.get(position).comment);


     }

    @Override
    public int getItemCount() {
        return adsArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name ,details;

        public ViewHolder(View itemView) {
            super(itemView);
            this.name=itemView.findViewById(R.id.name);
            this.details=itemView.findViewById(R.id.details);
        }
    }
}
