package com.yumaas.tknyaapp.students;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.RecyclerView;

import com.yumaas.tknyaapp.R;
import com.yumaas.tknyaapp.admin.AdminTeachersAdapter;
import com.yumaas.tknyaapp.base.DataBaseHelper;
import com.yumaas.tknyaapp.databinding.FragmentListBinding;
import com.yumaas.tknyaapp.models.User;

import java.util.ArrayList;

import static android.view.View.OnClickListener;


public class FragmentTeacherMembers extends Fragment {
    View rootView;
    FragmentListBinding fragmentListBinding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        fragmentListBinding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_list, container, false);
        RecyclerView recyclerView = fragmentListBinding.recyclerView;


        ArrayList<User> teachers = new ArrayList<>();
        ArrayList<User> users = DataBaseHelper.getDataLists().users;
        for (int i = 0; i < users.size(); i++) {
            if (DataBaseHelper.getDataLists().users.get(i).type == 5) {
                teachers.add(users.get(i));
            }
        }
        TeacherMembersAdapter menuAdapter = new TeacherMembersAdapter(getActivity(), teachers,false);
        recyclerView.setAdapter(menuAdapter);

        rootView = fragmentListBinding.getRoot();



        return rootView;
    }


}
