package com.yumaas.tknyaapp.students;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import com.yumaas.tknyaapp.R;


public class ActivityAdminStudentCategories extends AppCompatActivity {
    RecyclerView menuList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_categories);

        findViewById(R.id.iv_main_menu).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        menuList =  findViewById(R.id.rv_systems_list);
        StudentsCategoriesAdapter menuAdapter = new StudentsCategoriesAdapter(this);
        menuList.setAdapter(menuAdapter);


    }
}